# Устанавливаем SSL на сайты с Docker

https://miki725.com/docker/crypto/2017/01/29/docker+nginx+letsencrypt.html check this next time

Основано на статье https://www.humankode.com/ssl/how-to-set-up-free-ssl-certificates-from-lets-encrypt-using-docker-and-nginx

cd /home

git clone https://gitlab.com/wb_horev/ssl-cert-creator.git ssl-cert-creator

cd ssl-cert-creator

nano ngnix.conf и
```в поле server_name пишем хост для которого надо создать сертификаты```

docker-compose up -d

Проверяем что по введенному в конфиг адресу доступен сайт с демо страничкой

Запускаем тестовую команду:

```
sudo docker run -it --rm \
-v /home/docker-volumes/etc/letsencrypt:/etc/letsencrypt \
-v /home/docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt \
-v /home/ssl-cert-creator/app:/data/letsencrypt \
-v "/home/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" \
certbot/certbot \
certonly --webroot \
--register-unsafely-without-email --agree-tos \
--webroot-path=/data/letsencrypt \
--staging \
-d example.com -d www.example.com
```

Убеждаемся что в консоли будет положительный результат

Чистим результаты тестовго прогона

sudo rm -rf /home/docker-volumes/

Запрашиваем боевой сертификат

```
sudo docker run -it --rm \
-v /home/docker-volumes/etc/letsencrypt:/etc/letsencrypt \
-v /home/docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt \
-v /home/ssl-cert-creator/app:/data/letsencrypt \
-v "/home/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" \
certbot/certbot \
certonly --webroot \
--email sup@dkbx.ru --agree-tos --no-eff-email \
--webroot-path=/data/letsencrypt \
-d dkdev.pro -d www.dkdev.pro
```

Если все прошло ок - выключаем тестовый nginx контейнер

docker-compose down

### dh-param

Generate a 2048 bit DH Param file

sudo openssl dhparam -out /home/app...path/dh-param/dhparam-2048.pem 2048



### Подключаем сертификаты к продакшен сайту

в docker-compose.yml нужно замапить сертификаты в сервисе nginx

```
services:

  production-nginx-container:
    container_name: 'production-nginx-container'
    image: nginx:latest
    ports:
    - "80:80"
    - "443:443"
    volumes:
    - ./production.conf:/etc/nginx/conf.d/default.conf
    - ./app:/usr/share/nginx/html
    - ./dh-param/dhparam-2048.pem:/etc/ssl/certs/dhparam-2048.pem
    - /home/docker-volumes/etc/letsencrypt/live/dkdev.pro/fullchain.pem:/etc/letsencrypt/live/dkdev.pro/fullchain.pem
    - /home/docker-volumes/etc/letsencrypt/live/dkdev.pro/privkey.pem:/etc/letsencrypt/live/dkdev.pro/privkey.pem
```


стандартный конфиг для ssl nginx (production.conf)

```
server {
    listen      80;
    listen [::]:80;
    server_name dkdev.pro www.dkdev.pro;

    location / {
        rewrite ^ https://$host$request_uri? permanent;
    }

    #for certbot challenges (renewal process)
    location ~ /.well-known/acme-challenge {
        allow all;
        root /data/letsencrypt;
    }
}

#https://dkdev.pro
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name dkdev.pro;

    server_tokens off;

    ssl on;

    ssl_certificate /etc/letsencrypt/live/dkdev.pro/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/dkdev.pro/privkey.pem;

    ssl_buffer_size 8k;
    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8;

    root /usr/share/nginx/html;
    index index.html;
}

#https://www.dkdev.pro
server {
    server_name www.dkdev.pro;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_tokens off;

    ssl on;

    ssl_certificate /etc/letsencrypt/live/dkdev.pro/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/dkdev.pro/privkey.pem;

    ssl_buffer_size 8k;
    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8 8.8.4.4;

   return 301 https://dkdev.pro$request_uri;
}
```

# Делаем автообновление сертификатов

sudo crontab -e

добавляем строку (запуск каждый день в 23.00 и обновит если сертификат пора обновлять и перегрузит сервис production-nginx-container)

0 23 * * * docker run --rm -it --name certbot -v "/home/docker-volumes/etc/letsencrypt:/etc/letsencrypt" -v "/home/docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt" -v "/home/docker-volumes/data/letsencrypt:/data/letsencrypt" -v "/home/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" certbot/certbot renew --webroot -w /data/letsencrypt --quiet && docker kill --signal=HUP production-nginx-container

или с обновлением сервиса web/nginx

0 23 * * * docker run --rm -it --name certbot -v "/home/docker-volumes/etc/letsencrypt:/etc/letsencrypt" -v "/home/docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt" -v "/home/docker-volumes/data/letsencrypt:/data/letsencrypt" -v "/home/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" certbot/certbot renew --webroot -w /data/letsencrypt --quiet && docker service update --update-order start-first --image docker.p2pwatch.io/p2pw-web:latest p2pw_web

# Проверка сертификата

https://www.ssllabs.com/ssltest/analyze.html?d=dkdev.pro

# Улучшаем хедеры

https://securityheaders.com/?q=dkdev.pro&hide=on&followRedirects=on

в production.conf добавить

```
#https://www.ohhaithere.com
server {
    # ....

    location / {
        #security headers
        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload";
        add_header X-XSS-Protection "1; mode=block" always;
        add_header X-Content-Type-Options "nosniff" always;
        add_header X-Frame-Options "DENY" always;
        add_header Feature-Policy: "vibrate 'self'; usermedia *; sync-xhr 'self'";
        #CSP
        add_header Content-Security-Policy "frame-src 'self'; default-src 'self'; script-src 'self' 'unsafe-inline' https://maxcdn.bootstrapcdn.com https://ajax.googleapis.com; img-src 'self'; style-src 'self' https://maxcdn.bootstrapcdn.com; font-src 'self' data: https://maxcdn.bootstrapcdn.com; form-action 'self'; upgrade-insecure-requests;" always;
        add_header Referrer-Policy "strict-origin-when-cross-origin" always;
    }

    # ....
}
```